﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Disney_Princess.Models
{
    static class PrincessesConstParams
    {
        static readonly string[] HairColors = new string[6] { "Black", "Blonde", "Platinum-blonde", "Strawberry-blonde", "Red", "Brown" };
        static readonly string[] EyeColors = new string[4] { "Brown", "Blue", "Violet", "Hazel" };
    }
}

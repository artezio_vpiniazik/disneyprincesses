﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Disney_Princess.Models
{
    class Command
    {
        public string CommandName { get; set; }
        public Princess princess { get; set; }        
    }
}

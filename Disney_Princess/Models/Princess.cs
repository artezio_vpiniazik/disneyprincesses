﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Disney_Princess.Models.PrincessesConstParams;

namespace Disney_Princess.Models
{    
    class Princess
    {
        public int Number { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public string HairColor { get; set; }
        public string EyeColor{get;set;}
    }
}

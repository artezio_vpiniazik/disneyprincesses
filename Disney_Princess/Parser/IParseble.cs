﻿using Disney_Princess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Disney_Princess.Parser
{
    interface IParseble
    {
        Princess FromLineToPrincess(string line);
        Command FromLineToCommand(string line);
    }
}

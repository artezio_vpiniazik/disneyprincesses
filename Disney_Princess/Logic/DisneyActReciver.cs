﻿using Disney_Princess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Disney_Princess.Logic
{
    class DisneyActReciver : IActionGiver
    {    
        private List<Action> Actions = new List<Action>();     
        

        public DisneyActReciver()
        {                         
            Actions.Add(List);
            Actions.Add(Add);
            Actions.Add(Get);
            Actions.Add(Update);
            Actions.Add(Delete);
            Actions.Add(Exit);
        }

        public Action GiveCommand(Command cmd)
        {
            int? MinDist = null;
            Action result = null;
            foreach (var command in Actions)
            {
                if (MinDist > Comparer.GetDistance(command.Method.Name, cmd.CommandName) || MinDist == null)
                {
                    MinDist = Comparer.GetDistance(command.Method.Name, cmd.CommandName);
                    result = command.Invoke;
                }
            }
            return result;            
        }
        public void List(Command cmd)
        {
            foreach (var princess in CRUD.WorkPrincesses)
            {
                Console.WriteLine(">"+princess.Number+". "+princess.Name );
                Console.WriteLine(" Age:"+princess.Age);
                Console.WriteLine(" Hair:"+princess.HairColor);
                Console.WriteLine(" Eyes:"+princess.EyeColor);
                Console.WriteLine();
            }
        }
        public void Add(Command cmd)
        {
            if (cmd.princess.Name == null)
            {
                throw new FormatException("Not enought parametrs");
            }
            CRUD.WorkPrincesses.Add(cmd.princess);
            Console.WriteLine("> Princess “{0}” has been added.",cmd.princess.Name);
        }
        public void Get(Command cmd)
        {
            foreach (var princess in CRUD.WorkPrincesses)
            {
                if (princess.Number == cmd.princess.Number)
                {
                    Console.WriteLine("> "+princess.Name);
                    Console.WriteLine(" Age"+princess.Age);
                    Console.WriteLine(" Hair:"+princess.HairColor);
                    Console.WriteLine(" Eyes:"+princess.EyeColor);
                }
            }
        }
        public void Update(Command cmd)
        {
            if (cmd.princess.Name == null)
            {
                throw new FormatException("Not enought parametrs");
            }
            foreach (var princess in CRUD.WorkPrincesses)
            {
                if (princess.Number == cmd.princess.Number)
                {
                    CRUD.WorkPrincesses.Remove(princess);
                    break;
                }
            }            
            CRUD.WorkPrincesses.Add(cmd.princess);
            Console.WriteLine("> Princess “{0}” has been update.", cmd.princess.Name);
        }
        public void Delete(Command cmd)
        {
            foreach (var princess in CRUD.WorkPrincesses)
            {
                if (princess.Number == cmd.princess.Number)
                {
                    CRUD.WorkPrincesses.Remove(princess);
                    break;
                }
            }
            Console.WriteLine("> Princess “{0}” has been deleted.",cmd.princess.Name);
        }
        public void Exit(Command cmd)
        {
            CRUD.IsExit = true;        
        }
    }
}

﻿using Disney_Princess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Disney_Princess.Logic
{
    interface ICRUD
    {        
        void ExecuteCommand(Command command);
    }
}

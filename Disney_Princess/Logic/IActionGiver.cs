﻿using Disney_Princess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Disney_Princess.Logic
{
    delegate void Action(Command cmd);
    interface IActionGiver
    {
        Action GiveCommand(Command cmd);
    }
}

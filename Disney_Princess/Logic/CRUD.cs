﻿using Disney_Princess.DataManager;
using Disney_Princess.Models;
using Disney_Princess.Parser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Disney_Princess.Logic
{    
    class CRUD : ICRUD
    {             
        public static bool IsExit = false;
        public static List<Princess> WorkPrincesses;
        private IDataManager datamanager;
        private IParseble Parse;
        private IActionGiver actgiver;
        public CRUD(IDataManager datamanager,IParseble Parse,IActionGiver actgiver)
        {
            this.actgiver = actgiver;
            this.datamanager = datamanager;
            this.Parse = Parse;
            WorkPrincesses = datamanager.LoadPrincess();
            ListenCommand();
        }      
         
        public void ExecuteCommand(Command command)
        {
            Action act=actgiver.GiveCommand(command);
            act.Invoke(command);      
        }

        private void ListenCommand()
        {
            do
            {
                Console.Write("<");
                try
                {
                    ExecuteCommand(Parse.FromLineToCommand(Console.ReadLine()));
                }
                catch (FormatException e) { Console.WriteLine(">Error:"+ e.Message); }                
            }
            while (!IsExit);
        }
    }
}

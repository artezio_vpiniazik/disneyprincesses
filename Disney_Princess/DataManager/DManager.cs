﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Disney_Princess.Models;
using Disney_Princess.Parser;
using System.IO;

namespace Disney_Princess.DataManager
{
    class DManager : IDataManager
    {
        private IParseble Parse=new DisneyParser();
               
        public List<Princess> LoadPrincess()
        {
            List<Princess> result = new List<Princess>();            
                        
            using (StreamReader sr = new StreamReader(@"..\..\Reses\Princesses.txt"))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    result.Add(Parse.FromLineToPrincess(line));
                }                               
            }
            Console.WriteLine("Princesses successfully loaded!!!!!");
            return result;
        }
    }
}
